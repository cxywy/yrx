import requests
import time
from queue import Queue
import logging

# 配置日志模块
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(levelname)s : %(message)s')

PAGE = 100
COUNT = 0
queue = Queue()


class Js7():

    def __init__(self):
        self.PAGE = 10
        self.COUNT = 0
        self.queue = Queue()

    def jiexi(self, res_json):
        # logging.info('当前正在解析：%s', res_json)
        for _ in res_json['data']:
            queue.put(int(_['value']))

    def count(self):
        logging.info('当前正在计算加和')
        print(queue.qsize())
        while queue.qsize():
            self.COUNT += queue.get()
        return self.COUNT

    def fetch(self, page):
        cookies = {
            'Hm_lvt_337e99a01a907a08d00bed4a1a52e35d': '1610258207,1610285767,1610331111',
            'sessionid': '2khkm1x8bolp8g543irsct93k1yix2x6',
            'Hm_lpvt_337e99a01a907a08d00bed4a1a52e35d': str(int(time.time())),
        }

        headers = {
            'Connection': 'keep-alive',
            'Pragma': 'no-cache',
            'Cache-Control': 'no-cache',
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'X-Requested-With': 'XMLHttpRequest',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Origin': 'http://www.python-spider.com',
            'Referer': 'http://www.python-spider.com/challenge/7',
            'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7',
        }
        data = {
            'page': str(page)
        }
        logging.info('当前正在抓取第：%s 页', page)
        session = requests.Session()
        session.post('http://www.python-spider.com/cityjson', headers=headers, cookies=cookies, verify=False)
        response = session.post('http://www.python-spider.com/api/challenge7', headers=headers, cookies=cookies,
                                data=data,
                                verify=False)
        self.jiexi(response.json())


if __name__ == '__main__':
    js7 = Js7()
    for page in range(1, PAGE + 1):
        js7.fetch(page)
    print('运行即将结束')
    print(js7.count())