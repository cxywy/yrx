import requests
import time
import hashlib, base64

counter = 0

for page in range(1, 86):
    base_url = 'http://www.python-spider.com/challenge/api/json?page={}&count=14'

    temp_str = str(int(time.time()))
    base_str = '9622' + temp_str
    bytes_str = base_str.encode('utf-8')
    base_64 = base64.b64encode(bytes_str)

    headers = {
        'Proxy-Connection': 'keep-alive',
        'Cache-Control': 'no-cache',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36 Edg/88.0.705.81',
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Referer': 'http://www.python-spider.com/challenge/1',
        'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
        'Connection': 'keep-alive',
        'Sec-Fetch-Site': 'cross-site',
        'Sec-Fetch-Mode': 'no-cors',
        'Sec-Fetch-Dest': 'image',
        'If-None-Match': '0bc6db0fa42799d3f0166688cf502feb',
        'Content-Length': '0',
        'Origin': 'http://www.python-spider.com',
        'timestamp': temp_str,
        'X-Requested-With': 'XMLHttpRequest',
        'safe': hashlib.md5(base_64).hexdigest(),
        'Pragma': 'no-cache',
    }

    response = requests.get(base_url.format(page), headers=headers)
    print(response.json()['infos'])
    for _ in response.json()['infos']:
        if '招' in _['message']:
            counter += 1
    print(f'当前第{page}页')
    time.sleep(2)

print(counter)