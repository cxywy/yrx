import asyncio, aiohttp


async def main():
    async with aiohttp.ClientSession() as  session:
        counter = 0
        for page in range(1, 101):
            data = {
                'page': page
            }
            async with session.post('http://www.python-spider.com/api/challenge6', data=data) as response:
                result = await response.json()
                for value in result['data']:
                    counter += int(value['value'])

        print(counter)


if __name__ == '__main__':
    asyncio.run(main())