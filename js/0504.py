import requests
from ip_pool import ProxyHelper
import logging

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(levelname)s: %(message)s')


def fetch(data):
    proxy, version = helper.get()
    logging.info('The proxy：%s , The version: %s', proxy, version)
    proxies = {
        "http": "http://" + proxy,
        "https": "https://" + proxy,
    }
    try:
        response = requests.post('http://www.python-spider.com/api/challenge4', data=data, proxies=proxies)
        if response.status_code != 200:
            helper.update(version)
            response = fetch(data)
            return response
        else:
            return response.json()
    except:
        helper.update(version)
        response = fetch(data)
        return response


def main():
    counter = 0
    # 增量访问接口
    for page in range(1, 101):
        data = {
            'page': page
        }
        response_json = fetch(data)
        # 遍历value
        logging.info('正在抓取 第 %s 页 , 当前 counter 值：%s', page, counter)
        for value in response_json['data']:
            counter += int(value['value'].strip('\r'))
    print(counter)


if __name__ == '__main__':
    helper = ProxyHelper()
    main()