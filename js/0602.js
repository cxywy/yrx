function get_cookie() {

    window = this;
    var hexcase = 0;
    var chrsz = 8;

    function md5_ii(a, b, c, d, x, s, t) {
        return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
    }

    function md5_hh(a, b, c, d, x, s, t) {
        return md5_cmn(b ^ c ^ d, a, b, x, s, t);
    }

    function md5_gg(a, b, c, d, x, s, t) {
        return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
    }

    function bit_rol(num, cnt) {
        return (num << cnt) | (num >>> (32 - cnt));
    }

    function safe_add(x, y) {
        var lsw = (x & 0xFFFF) + (y & 0xFFFF);
        var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
        return (msw << 16) | (lsw & 0xFFFF);
    }

    function md5_cmn(q, a, b, x, s, t) {
        return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s), b);
    }

    function md5_ff(a, b, c, d, x, s, t) {
        return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
    }

    function binl2hex(binarray) {
        var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
        var str = "";
        for (var i = 0; i < binarray.length * 4; i++) {
            str += hex_tab.charAt((binarray[i >> 2] >> ((i % 4) * 8 + 4)) & 0xF) +
                hex_tab.charAt((binarray[i >> 2] >> ((i % 4) * 8)) & 0xF);
        }
        return str;
    }

    function core_md5(x, len) {
        /* append padding */
        x[len >> 5] |= 0x80 << ((len) % 32);
        x[(((len + 64) >>> 9) << 4) + 14] = len;

        var a = 1732584193;
        var b = -271733879;
        var c = -1732584194;
        var d = 271733878;

        for (var i = 0; i < x.length; i += 16) {
            var olda = a;
            var oldb = b;
            var oldc = c;
            var oldd = d;

            a = md5_ff(a, b, c, d, x[i + 0], 7, -680876936);
            d = md5_ff(d, a, b, c, x[i + 1], 12, -389564586);
            c = md5_ff(c, d, a, b, x[i + 2], 17, 606105819);
            b = md5_ff(b, c, d, a, x[i + 3], 22, -1044525330);
            a = md5_ff(a, b, c, d, x[i + 4], 7, -176418897);
            d = md5_ff(d, a, b, c, x[i + 5], 12, 1200080426);
            c = md5_ff(c, d, a, b, x[i + 6], 17, -1473231341);
            b = md5_ff(b, c, d, a, x[i + 7], 22, -45705983);
            a = md5_ff(a, b, c, d, x[i + 8], 7, 1770035416);
            d = md5_ff(d, a, b, c, x[i + 9], 12, -1958414417);
            c = md5_ff(c, d, a, b, x[i + 10], 17, -42063);
            b = md5_ff(b, c, d, a, x[i + 11], 22, -1990404162);
            a = md5_ff(a, b, c, d, x[i + 12], 7, 1804603682);
            d = md5_ff(d, a, b, c, x[i + 13], 12, -40341101);
            c = md5_ff(c, d, a, b, x[i + 14], 17, -1502002290);
            b = md5_ff(b, c, d, a, x[i + 15], 22, 1236535329);

            a = md5_gg(a, b, c, d, x[i + 1], 5, -165796510);
            d = md5_gg(d, a, b, c, x[i + 6], 9, -1069501632);
            c = md5_gg(c, d, a, b, x[i + 11], 14, 643717713);
            b = md5_gg(b, c, d, a, x[i + 0], 20, -373897302);
            a = md5_gg(a, b, c, d, x[i + 5], 5, -701558691);
            d = md5_gg(d, a, b, c, x[i + 10], 9, 38016083);
            c = md5_gg(c, d, a, b, x[i + 15], 14, -660478335);
            b = md5_gg(b, c, d, a, x[i + 4], 20, -405537848);
            a = md5_gg(a, b, c, d, x[i + 9], 5, 568446438);
            d = md5_gg(d, a, b, c, x[i + 14], 9, -1019803690);
            c = md5_gg(c, d, a, b, x[i + 3], 14, -187363961);
            b = md5_gg(b, c, d, a, x[i + 8], 20, 1163531501);
            a = md5_gg(a, b, c, d, x[i + 13], 5, -1444681467);
            d = md5_gg(d, a, b, c, x[i + 2], 9, -51403784);
            c = md5_gg(c, d, a, b, x[i + 7], 14, 1735328473);
            b = md5_gg(b, c, d, a, x[i + 12], 20, -1926607734);

            a = md5_hh(a, b, c, d, x[i + 5], 4, -378558);
            d = md5_hh(d, a, b, c, x[i + 8], 11, -2022574463);
            c = md5_hh(c, d, a, b, x[i + 11], 16, 1839030562);
            b = md5_hh(b, c, d, a, x[i + 14], 23, -35309556);
            a = md5_hh(a, b, c, d, x[i + 1], 4, -1530992060);
            d = md5_hh(d, a, b, c, x[i + 4], 11, 1272893353);
            c = md5_hh(c, d, a, b, x[i + 7], 16, -155497632);
            b = md5_hh(b, c, d, a, x[i + 10], 23, -1094730640);
            a = md5_hh(a, b, c, d, x[i + 13], 4, 681279174);
            d = md5_hh(d, a, b, c, x[i + 0], 11, -358537222);
            c = md5_hh(c, d, a, b, x[i + 3], 16, -722521979);
            b = md5_hh(b, c, d, a, x[i + 6], 23, 76029189);
            a = md5_hh(a, b, c, d, x[i + 9], 4, -640364487);
            d = md5_hh(d, a, b, c, x[i + 12], 11, -421815835);
            c = md5_hh(c, d, a, b, x[i + 15], 16, 530742520);
            b = md5_hh(b, c, d, a, x[i + 2], 23, -995338651);

            a = md5_ii(a, b, c, d, x[i + 0], 6, -198630844);
            d = md5_ii(d, a, b, c, x[i + 7], 10, 1126891415);
            c = md5_ii(c, d, a, b, x[i + 14], 15, -1416354905);
            b = md5_ii(b, c, d, a, x[i + 5], 21, -57434055);
            a = md5_ii(a, b, c, d, x[i + 12], 6, 1700485571);
            d = md5_ii(d, a, b, c, x[i + 3], 10, -1894986606);
            c = md5_ii(c, d, a, b, x[i + 10], 15, -1051523);
            b = md5_ii(b, c, d, a, x[i + 1], 21, -2054922799);
            a = md5_ii(a, b, c, d, x[i + 8], 6, 1873313359);
            d = md5_ii(d, a, b, c, x[i + 15], 10, -30611744);
            c = md5_ii(c, d, a, b, x[i + 6], 15, -1560198380);
            b = md5_ii(b, c, d, a, x[i + 13], 21, 1309151649);
            a = md5_ii(a, b, c, d, x[i + 4], 6, -145523070);
            d = md5_ii(d, a, b, c, x[i + 11], 10, -1120210379);
            c = md5_ii(c, d, a, b, x[i + 2], 15, 718787259);
            b = md5_ii(b, c, d, a, x[i + 9], 21, -343485551);

            a = safe_add(a, olda);
            b = safe_add(b, oldb);
            c = safe_add(c, oldc);
            d = safe_add(d, oldd);
        }
        return Array(a, b, c, d);

    }

    function str2binl(str) {
        var bin = Array();
        var mask = (1 << chrsz) - 1;
        for (var i = 0; i < str.length * chrsz; i += chrsz)
            bin[i >> 5] |= (str.charCodeAt(i / chrsz) & mask) << (i % 32);
        return bin;
    }

    function hex_md5(s) {
        return binl2hex(core_md5(str2binl(s), s.length * chrsz));
    }


    var _$oa = [
        "b0pzbVk=",
        "dmFsdWVPZg==",
        "bG9n",
        "ZnVuY3Rpb24gKlwoICpcKQ==",
        "dFZuWW4=",
        "cnN1SEQ=",
        "Z2dlcg==",
        "Y29uc3RydWN0b3I=",
        "Z3BPR2I=",
        "eEVuZmY=",
        "UFNiS0Y=",
        "cmVsb2Fk",
        "R0h6UWg=",
        "aWltaXk=",
        "T0VSWUs=",
        "TU1RVms=",
        "eEVObms=",
        "WVlxaWg=",
        "YWN0aW9u",
        "bUtuZWw=",
        "aU1aQWQ=",
        "YnRvYQ==",
        "c3RhdGVPYmplY3Q=",
        "VUNnTFk=",
        "RlVzd00=",
        "dFh3ckY=",
        "aW5wdXQ=",
        "c3RyaW5n",
        "SUtITGI=",
        "eURCQm8=",
        "RE9YRVg=",
        "a1J0R0Y=",
        "eU9DTmY=",
        "YUR3U08=",
        "Y2hhaW4=",
        "WkprZlA=",
        "Wk10dHQ=",
        "SVlqY3I=",
        "Qnh6aGg=",
        "eGJrQ2M=",
        "5q2k572R6aG15Y+X44CQ54ix6ZSt5LqR55u+IFYxLjAg5Yqo5oCB54mI44CR5L+d5oqk",
        "c2lnbj0=",
        "d3hFU2E=",
        "bGVuZ3Ro",
        "TVljamQ=",
        "UXZqUEg=",
        "VVRwclo=",
        "QnlScHQ=",
        "dGVzdA==",
        "cmVTcU0=",
        "enlqZVY=",
        "WlRUZ0U=",
        "WEJxTXI=",
        "QVB4bk4=",
        "ZEhManI=",
        "SWR4eU0=",
        "TmFQeHU=",
        "Y29va2ll",
        "YmxURXo=",
        "TGpJYmo=",
        "S056b2U=",
        "UHdPdXk=",
        "WVlDWUg=",
        "dlV2SVc=",
        "Q2x3QVg=",
        "U3dpT0c=",
        "cm91bmQ=",
        "THVZSmc=",
        "YlV5SVQ=",
        "Q05WalQ=",
        "alhiZGY=",
        "ZWZLTEs=",
        "bE54dnU=",
        "TUdrY3I=",
        "VnhlaUY=",
        "OyBwYXRoPS8=",
        "aWVwTWY=",
        "R1ZKU3A=",
        "Y2FsbA==",
        "YkREaFI=",
        "VnJ2V1c=",
        "WmVtU2o=",
        "d2hpbGUgKHRydWUpIHt9",
        "bGtjZ04=",
        "WWFmcXg=",
        "SEVjV24=",
        "a0FFWmg=",
        "eWZNd2k=",
        "akp2T3I=",
        "d3dIbFE=",
        "Vnh2c2o=",
        "Q3FmUWc=",
        "dWxEU3E=",
        "TndRUU0=",
        "T2xMVHg=",
        "Q1VTc1k=",
        "YXBwbHk=",
        "bHlzYW4=",
        "Z1VqR1c=",
        "ZllxQkQ=",
        "RHNzcks=",
        "SlVvcnM=",
        "VVlhYmo=",
        "YlNwZUw=",
        "R25KRU0=",
        "ZGVidQ==",
        "XCtcKyAqKD86W2EtekEtWl8kXVswLTlhLXpBLVpfJF0qKQ==",
        "cFV2Ymw=",
        "TVROVlg=",
        "RWhOaUE=",
        "aW5pdA==",
        "Snp4YlA=",
        "SmF1aE0=",
        "elR0Wmo=",
        "T2Z2WWk=",
        "YWlkaW5nX3dpbg==",
        "TXJ6cEg="
    ]
    var _$ob = function (a, b) {
        a = a - 0x0;
        var c = _$oa[a];
        if (_$ob['ibfjjf'] === undefined) {
            (function () {
                var f;
                try {
                    var h = Function('return\x20(function()\x20' + '{}.constructor(\x22return\x20this\x22)(\x20)' + ');');
                    f = h();
                } catch (i) {
                    f = window;
                }
                var g = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
                f['atob'] || (f['atob'] = function (j) {
                        var k = String(j)['replace'](/=+$/, '');
                        var l = '';
                        for (var m = 0x0, n, o, p = 0x0; o = k['charAt'](p++); ~o && (n = m % 0x4 ? n * 0x40 + o : o,
                        m++ % 0x4) ? l += String['fromCharCode'](0xff & n >> (-0x2 * m & 0x6)) : 0x0) {
                            o = g['indexOf'](o);
                        }
                        return l;
                    }
                );
            }());
            _$ob['ieVoMz'] = function (e) {
                var f = atob(e);
                var g = [];
                for (var h = 0x0, j = f['length']; h < j; h++) {
                    g += '%' + ('00' + f['charCodeAt'](h)['toString'](0x10))['slice'](-0x2);
                }
                return decodeURIComponent(g);
            }
            ;
            _$ob['lFPyFH'] = {};
            _$ob['ibfjjf'] = !![];
        }
        var d = _$ob['lFPyFH'][a];
        if (d === undefined) {
            c = _$ob['ieVoMz'](c);
            _$ob['lFPyFH'][a] = c;
        } else {
            c = d;
        }
        return c;
    };
    var a = {
        'GHzQh': _$ob('0x3'),
        'wwHlQ': _$ob('0x6a'),
        'bSpeL': function (d, e) {
            return d(e);
        },
        'bDDhR': _$ob('0x6e'),
        'lkcgN': function (d, e) {
            return d + e;
        },
        'MGkcr': _$ob('0x22'),
        'NwQQM': _$ob('0x1a'),
        'jXbdf': function (d) {
            return d();
        },
        'yOCNf': function (d, e) {
            return d === e;
        },
        'LuYJg': _$ob('0x47'),
        'iKrGd': _$ob('0xa'),
        'SwiOG': function (d, e) {
            return d + e;
        },
        'vUvIW': 'debu',
        'Bxzhh': 'gger',
        'pARCJ': _$ob('0x12'),
        'CqfQg': function (d, e) {
            return d !== e;
        },
        'JkubD': 'KPAjY',
        'iimiy': function (d, e) {
            return d(e);
        },
        'kRtGF': function (d, e) {
            return d + e;
        },
        'OfvYi': function (d, e) {
            return d(e);
        },
        'pITPH': function (d, e) {
            return d === e;
        },
        'yDBBo': _$ob('0x55'),
        'GpDsE': function (d, e, f) {
            return d(e, f);
        },
        'UTprZ': _$ob('0x28'),
        'jJvOr': function (d, e) {
            return d + e;
        },
        'THRFh': _$ob('0x73'),
        'yfMwi': function (d, e) {
            return d(e);
        },
        'QJaDX': function (d, e) {
            return d(e);
        },
        'tHZuz': function (d, e) {
            return d(e);
        },
        'iepMf': function (d, e) {
            return d / e;
        },
        'pUvbl': function (d, e) {
            return d + e;
        },
        'NMSMB': function (d, e) {
            return d + e;
        },
        'CNVjT': function (d, e) {
            return d + e;
        },
        'VxeiF': _$ob('0x29'),
        'IKHLb': function (d, e) {
            return d / e;
        },
        'ZJkfP': _$ob('0x4b')
    };

    var c = new Date()[_$ob('0x1')]();
    window.btoa = require('btoa')
    token = window[_$ob('0x15')](a[_$ob('0x58')](a['THRFh'], a[_$ob('0x57')](String, c)));
    md = a['QJaDX'](hex_md5, window['btoa'](a['jJvOr'](a['THRFh'], a['tHZuz'](String, Math[_$ob('0x42')](a[_$ob('0x4c')](c, 0x3e8))))));
    cookie = a['pUvbl'](a[_$ob('0x6b')](a[_$ob('0x6b')](a['pUvbl'](a['NMSMB'](a[_$ob('0x45')](a[_$ob('0x4a')], Math[_$ob('0x42')](a[_$ob('0x1c')](c, 0x3e8))), '~'), token), '|'), md), a[_$ob('0x23')]);
    return cookie
}

console.log(get_cookie())