import requests, execjs

session = requests.session()

with open('0602.js', 'r', encoding='utf-8')as f:
    sign = f.read()
cookie = execjs.compile(sign).call('get_cookie')

cookies = {
    'sessionid': '3jz53x3j9uuex0kncbvkdu6iwkts08w0',
    'sign': cookie.split(';')[0].strip('sign=')
}
print(cookies)
#
url = 'http://www.python-spider.com/challenge/2'
resp = session.get(url, cookies=cookies)
print(resp.content.decode())